Mdg Payment Gateway
=======================

Dummy payment gateway. 
Implemented 4 gateway endpoints:\

####Authorize request
```
Endpoint: 
https://%baseurl%/rest/V1/mdg/payment/authorize

Request Params:
{
    "ccNum": "4111111111111111",
    "ccHolder": "John Doe",
    "ccExp": "07/28",
    "ccCvv": "123",
    "amount": "101",
    "currency": "USD",
    "merchantTnRef": "1000000123",
    "comment": "Order 1000000123"
}
```

####Capture request
```
Endpoint: 
https://%baseurl%/rest/V1/mdg/payment/capture

Request Params:
{
    "tnRef": "5be455d372880",
    "comment": "Order 1000000123"
}
```
####Authorize and capture request
```
Endpoint:
https://%baseurl%/rest/V1/mdg/payment/authcapture

Request Params:
{
    "ccNum": "4111111111111111",
    "ccHolder": "John Doe",
    "ccExp": "07/28",
    "ccCvv": "123",
    "amount": "101",
    "currency": "USD",
    "merchantTnRef": "1000000123",
    "comment": "Order 1000000123"
}
```
####Refund request
```
Endpoint:
https://%baseurl%/rest/V1/mdg/payment/refund

Request params:
{
    "tnRef": "5be455d372880",
    "amount": "100",
    "currency": "USD"
}

```

####Void request
```
Endpoint:
https://%baseurl%/rest/V1/mdg/payment/void

Request params:
{
    "tnRef": "5be455d372880"
}
```
