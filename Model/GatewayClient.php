<?php
/**
 * @author Misha Medzhytov
 * @copyright Copyright (c) 2011-2018 Mdg, Inc. (http://www.medzhytov.com)
 */

namespace Mdg\PaymentGateway\Model;

use Mdg\PaymentGateway\Helper\Data;
use Zend\Http\Client;
use Zend\Http\RequestFactory;

/**
 * Class GatewayClient
 *
 * @package Mdg\PaymentGateway\Model
 */
class GatewayClient implements \Mdg\PaymentGateway\Api\GatewayClientInterface
{
    /**
     * @var \Zend\Http\Client
     */
    private $client;

    /**
     * @var Data
     */
    private $helper;

    /**
     * HttpClient constructor.
     *
     * @param Client $client
     * @param Data $helper
     */
    public function __construct(
        \Zend\Http\Client $client,
        Data $helper
    )
    {
        $this->client = $client;
        $this->client->setAdapter(\Zend\Http\Client\Adapter\Curl::class);
        $this->client->setOptions(['curloptions' => [
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_FRESH_CONNECT => 1,
        ]]);
        $this->helper = $helper;
    }

    /**
     * @return Client
     */
    private function getClient()
    {
        return $this->client;
    }

    /**
     * @param string $ccNum
     * @param string $ccHolder
     * @param string $ccExpMonth
     * @param string $ccExpYear
     * @param string $ccCvv
     * @param float $amount
     * @param string $currency
     * @param string $merchantTnRef
     * @param string $comment
     * @return array
     */
    public function authorize(
        $ccNum,
        $ccHolder,
        $ccExpMonth,
        $ccExpYear,
        $ccCvv,
        $amount,
        $currency,
        $merchantTnRef,
        $comment = ''
    )
    {

        $requestData = [
            "ccNum" => $ccNum,
            "ccHolder" => $ccHolder,
            "ccExpMonth" => $ccExpMonth,
            "ccExpYear" => $ccExpYear,
            "ccCvv" => $ccCvv,
            "amount" => $amount,
            "currency" => $currency,
            "merchantTnRef" => $merchantTnRef,
            "comment" => $comment
        ];

        return $this->doRequest('authorize', $requestData);
    }

    /**
     * @param string $tnRef
     * @param string $comment
     * @return array
     */
    public function capture(
        $tnRef,
        $comment = ''
    )
    {
        $requestData = [
            "tnRef" => $tnRef,
            "comment" => $comment
        ];

        return $this->doRequest('capture', $requestData);
    }

    /**
     * @param string $tnRef
     * @param float $amount
     * @param string $currency
     * @return array
     */
    public function refund(
        $tnRef,
        $amount,
        $currency
    )
    {
        $requestData = [
            "tnRef" => $tnRef,
            "amount" => $amount,
            "currency" => $currency
        ];

        return $this->doRequest('refund', $requestData);
    }

    /**
     * @param string $tnRef
     * @return array
     */
    public function void($tnRef)
    {
        $requestData = [
            "tnRef" => $tnRef,
        ];

        return $this->doRequest('void', $requestData);
    }

    /**
     * @param string $ccNum
     * @param string $ccHolder
     * @param string $ccExpMonth
     * @param string $ccExpYear
     * @param string $ccCvv
     * @param float $amount
     * @param string $currency
     * @param string $merchantTnRef
     * @param string $comment
     * @return array
     */
    public function authcapture(
        $ccNum,
        $ccHolder,
        $ccExpMonth,
        $ccExpYear,
        $ccCvv,
        $amount,
        $currency,
        $merchantTnRef,
        $comment = ''
    )
    {
        $requestData = [
            "ccNum" => $ccNum,
            "ccHolder" => $ccHolder,
            "ccExpMonth" => $ccExpMonth,
            "ccExpYear" => $ccExpYear,
            "ccCvv" => $ccCvv,
            "amount" => $amount,
            "currency" => $currency,
            "merchantTnRef" => $merchantTnRef,
            "comment" => $comment
        ];

        return $this->doRequest('authcapture', $requestData);
    }

    /**
     * @param Request $request
     * @param string $method
     * @return array
     */
    private function doRequest(
        $action,
        $data,
        $method = 'POST'
    )
    {
        $this->client->setUri($this->getActionUrl($action));
        $this->client->setMethod($method);
        $this->client->setRawBody(json_encode($data));
        $this->client->setEncType('application/json');
        $httpResponse = $this->getClient()->send();

        if (!$httpResponse->isSuccess()) {
            throw new \Zend\Json\Server\Exception\HttpException(
                $httpResponse->getReasonPhrase(),
                $httpResponse->getStatusCode()
            );
        }

        return json_decode($httpResponse->getBody(), true);
    }

    /**
     * Get action request url
     *
     * @param string $action
     * @return string
     */
    private function getActionUrl($action)
    {
        return $this->helper->getBaseUri() . $action;
    }
}
