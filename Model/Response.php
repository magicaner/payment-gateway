<?php
/**
 * @author Misha Medzhytov
 * @copyright Copyright (c) 2011-2018 Mdg, Inc. (http://www.medzhytov.com)
 */
namespace Mdg\PaymentGateway\Model;

use Mdg\PaymentGateway\Api\ResponseInterface;
use Magento\Framework\DataObject;

/**
 * Class Response
 *
 * @package Mdg\PaymentGateway\Model
 */
class Response extends DataObject implements ResponseInterface
{
    /**
     * Response constructor.
     *
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        parent::__construct($data);
        $this->setSuccess();
        $this->setCode('0');
    }

    /**
     * @param boolean $success
     * @return $this
     */
    public function setSuccess($success = true)
    {
        $this->setData('success', $success);
        return $this;
    }

    /**
     * @return boolean
     */
    public function getSuccess()
    {
        return $this->getData('success');
    }

    /**
     * @param \Mdg\PaymentGateway\Api\TransactionInterface $transaction
     * @return void
     */
    public function setTransaction($transaction)
    {
        $this->setData('transaction', $transaction);
    }

    /**
     * @return \Mdg\PaymentGateway\Api\TransactionInterface
     */
    public function getTransaction()
    {
        return $this->getData('transaction');
    }

    /**
     * @param string $message
     * @return void
     */
    public function setMessage($message)
    {
        $this->setData('message', $message);
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->getData('message');
    }

    /**
     * @param string $code
     * @return $this
     */
    public function setCode($code)
    {
        $this->setData('code', $code);
        return $this;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->getData('code');
    }
}
