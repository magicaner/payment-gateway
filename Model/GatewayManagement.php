<?php
/**
 * @author Misha Medzhytov
 * @copyright Copyright (c) 2011-2018 Mdg, Inc. (http://www.medzhytov.com)
 */
namespace Mdg\PaymentGateway\Model;

use Mdg\PaymentGateway\Api\GatewayManagementInterface;
use Mdg\PaymentGateway\Api\ResponseInterface;
use Mdg\PaymentGateway\Api\ResponseInterfaceFactory;
use Mdg\PaymentGateway\Api\TransactionInterface;
use Mdg\PaymentGateway\Api\TransactionInterfaceFactory;

/**
 * Class GatewayManagement
 *
 * @package Mdg\PaymentGateway\Model
 */
class GatewayManagement implements GatewayManagementInterface
{
    /**
     * @var TransactionInterfaceFactory
     */
    private $transactionFactory;
    /**
     * @var ResponseInterfaceFactory
     */
    private $responseFactory;

    /**
     * GatewayManagement constructor.
     *
     * @param TransactionInterfaceFactory $transactionFactory
     * @param ResponseInterfaceFactory $responseFactory
     */
    public function __construct(
        TransactionInterfaceFactory $transactionFactory,
        ResponseInterfaceFactory $responseFactory
    ) {
        $this->transactionFactory = $transactionFactory;
        $this->responseFactory = $responseFactory;
    }

    /**
     * @api
     * @param string $ccNum
     * @param string $ccHolder
     * @param string $ccExpMonth
     * @param string $ccExpYear
     * @param string $ccCvv
     * @param float $amount
     * @param string $currency
     * @param string $merchantTnRef
     * @param string $comment
     * @return \Mdg\PaymentGateway\Api\ResponseInterface
     */
    public function authorize(
        $ccNum,
        $ccHolder,
        $ccExpMonth,
        $ccExpYear,
        $ccCvv,
        $amount,
        $currency,
        $merchantTnRef,
        $comment
    ) {
        /** @var TransactionInterface $transaction */
        $transaction = $this->transactionFactory->create();
        /** @var ResponseInterface $response */
        $response = $this->responseFactory->create();

        $reference = uniqid();
        $transaction->setType(TransactionInterface::TYPE_AUTHORIZE);
        $transaction->setTnRef($reference);
        $transaction->setMerchantTnRef($merchantTnRef);
        $transaction->setAmount($amount);
        $transaction->setCurrency($currency);
        $transaction->setComment($comment);

        $response->setTransaction($transaction);
        $response->setMessage($amount . ' ' . $currency . ' has been successfully preauthorized');
        return $response;
    }

    /**
     * @api
     * @param string $tnRef
     * @param string $comment
     * @return \Mdg\PaymentGateway\Api\ResponseInterface
     */
    public function capture(
        $tnRef,
        $comment
    ) {
        /** @var TransactionInterface $transaction */
        $transaction = $this->transactionFactory->create();
        /** @var ResponseInterface $response */
        $response = $this->responseFactory->create();

        $reference = uniqid();
        $transaction->setType(TransactionInterface::TYPE_CAPTURE);
        $transaction->setTnRef($reference);
        $transaction->setParentTnRef($tnRef);
        $transaction->setComment($comment);

        $response->setTransaction($transaction);
        $response->setMessage('Transaction ' . $tnRef . ' has been successfully captured');
        return $response;
    }

    /**
     * @api
     * @param string $tnRef
     * @return \Mdg\PaymentGateway\Api\ResponseInterface
     */
    public function void($tnRef)
    {
        /** @var TransactionInterface $transaction */
        $transaction = $this->transactionFactory->create();
        /** @var ResponseInterface $response */
        $response = $this->responseFactory->create();

        $reference = uniqid();
        $transaction->setType(TransactionInterface::TYPE_VOID);
        $transaction->setTnRef($reference);
        $transaction->setParentTnRef($tnRef);

        $response->setTransaction($transaction);
        $response->setMessage('Transaction ' . $tnRef . ' has been successfully voided');
        return $response;
    }

    /**
     * @api
     * @param string $tnRef
     * @param float $amount
     * @param string $currency
     * @return \Mdg\PaymentGateway\Api\ResponseInterface
     */
    public function refund(
        $tnRef,
        $amount,
        $currency
    ) {
        /** @var TransactionInterface $transaction */
        $transaction = $this->transactionFactory->create();
        /** @var ResponseInterface $response */
        $response = $this->responseFactory->create();

        $reference = uniqid();
        $transaction->setType(TransactionInterface::TYPE_REFUND);
        $transaction->setTnRef($reference);
        $transaction->setParentTnRef($tnRef);
        $transaction->setAmount($amount);
        $transaction->setCurrency($currency);
        $response->setTransaction($transaction);
        $response->setMessage($amount . ' ' . $currency . ' has been successfully refunded');
        return $response;
    }

    /**
     * @api
     * @param string $ccNum
     * @param string $ccHolder
     * @param string $ccExpMonth
     * @param string $ccExpYear
     * @param string $ccCvv
     * @param float $amount
     * @param string $currency
     * @param string $merchantTnRef
     * @param string $comment
     * @return \Mdg\PaymentGateway\Api\ResponseInterface
     */
    public function authcapture(
        $ccNum,
        $ccHolder,
        $ccExpMonth,
        $ccExpYear,
        $ccCvv,
        $amount,
        $currency,
        $merchantTnRef,
        $comment
    ) {
        /** @var TransactionInterface $transaction */
        $transaction = $this->transactionFactory->create();
        /** @var ResponseInterface $response */
        $response = $this->responseFactory->create();

        $reference = uniqid();
        $transaction->setType(TransactionInterface::TYPE_AUTHCAPTURE);
        $transaction->setTnRef($reference);
        $transaction->setMerchantTnRef($merchantTnRef);
        $transaction->setAmount($amount);
        $transaction->setCurrency($currency);
        $transaction->setComment($comment);

        $response->setTransaction($transaction);
        $response->setMessage($amount . ' ' . $currency . ' has been successfully captured');
        return $response;
    }
}
