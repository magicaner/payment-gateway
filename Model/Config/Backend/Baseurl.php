<?php
/**
 * @author Misha Medzhytov
 * @copyright Copyright (c) 2011-2018 Mdg, Inc. (http://www.medzhytov.com)
 */

namespace Mdg\PaymentGateway\Model\Config\Backend;

use Mdg\PaymentGateway\Helper\Data;

/**
 * Class Baseurl
 *
 * @package Mdg\PaymentGateway\Model\Config\Backend
 */
class Baseurl extends \Magento\Framework\App\Config\Value implements \Magento\Framework\App\Config\ValueInterface
{
    /**
     * @var Data
     */
    private $helper;

    /**
     * Baseurl constructor.
     *
     * @param Data $helper
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\App\Config\ScopeConfigInterface $config,
        \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList,
        Data $helper
    ) {
        $this->helper = $helper;
        parent::__construct($context, $registry, $config, $cacheTypeList);
    }

    /**
     * @param string $key
     * @return string
     */
    public function getFieldsetDataValue($key)
    {
        return $this->helper->getBaseUri();
    }

    /**
     * @return string
     */
    public function getOldValue()
    {
        return $this->helper->getBaseUri();
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->helper->getBaseUri();
    }

    /**
     * @return bool
     */
    public function isValueChanged()
    {
        return false;
    }
}
