<?php
/**
 * @author Misha Medzhytov
 * @copyright Copyright (c) 2011-2018 Mdg, Inc. (http://www.medzhytov.com)
 */
namespace Mdg\PaymentGateway\Model;

use Mdg\PaymentGateway\Api\TransactionInterface;
use Magento\Framework\DataObject;

/**
 * Class Transaction
 *
 * @package Mdg\PaymentGateway\Model
 */
class Transaction extends DataObject implements TransactionInterface
{
    /**
     * @return string
     */
    public function getTnRef()
    {
        return $this->getData('tn_ref');
    }

    /**
     * @param string $tnRef
     * @return void
     */
    public function setTnRef($tnRef)
    {
        $this->setData('tn_ref', $tnRef);
    }

    /**
     * @return float
     */
    public function getAmount()
    {
        return $this->getData('amount');
    }

    /**
     * @param float $amount
     * @return void
     */
    public function setAmount($amount)
    {
        $this->setData('amount', $amount);
    }

    /**
     * @return string
     */
    public function getCurrency()
    {
        return $this->getData('currency');
    }


    /**
     * @param string $currency
     * @return void
     */
    public function setCurrency($currency)
    {
        $this->setData('currency', $currency);
    }

    /**
     * @return string
     */
    public function getComment()
    {
        return $this->getData('comment');
    }


    /**
     * @param string $comment
     * @return void
     */
    public function setComment($comment)
    {
        $this->setData('comment', $comment);
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->getData('type');
    }

    /**
     * @param string $type
     * @return void
     */
    public function setType($type)
    {
        $this->setData('type', $type);
    }

    /**
     * @return string
     */
    public function getParentTnRef()
    {
        return $this->getData('parent_tn_ref');
    }

    /**
     * @param string $parentTnRef
     * @return void
     */
    public function setParentTnRef($parentTnRef)
    {
        $this->setData('parent_tn_ref', $parentTnRef);
    }

    /**
     * @return string
     */
    public function getMerchantTnRef()
    {
        return $this->getData('merchant_tn_ref');
    }

    /**
     * @param string $merchant
     * @return void
     */
    public function setMerchantTnRef($merchantTnRef)
    {
        $this->setData('merchant_tn_ref', $merchantTnRef);
    }
}
