<?php
/**
 * @author Misha Medzhytov
 * @copyright Copyright (c) 2011-2018 Mdg, Inc. (http://www.medzhytov.com)
 */
namespace Mdg\PaymentGateway\Api;

use Mdg\PaymentGateway\Api\TransactionInterface;

/**
 * Interface GatewayManagementInterface
 *
 * @package Mdg\PaymentGateway\Api
 */
interface GatewayManagementInterface
{
    /**
     * Authorize request.
     *
     * @api
     * @param string $ccNum
     * @param string $ccHolder
     * @param string $ccExpMonth
     * @param string $ccExpYear
     * @param string $ccCvv
     * @param float $amount
     * @param string $currency
     * @param string $merchantTnRef
     * @param string $comment
     * @return \Mdg\PaymentGateway\Api\ResponseInterface
     */
    public function authorize(
        $ccNum,
        $ccHolder,
        $ccExpMonth,
        $ccExpYear,
        $ccCvv,
        $amount,
        $currency,
        $merchantTnRef,
        $comment
    );

    /**
     *
     * Capture transaction.
     *
     * @api
     * @param string $tnRef
     * @param string $comment
     * @return \Mdg\PaymentGateway\Api\ResponseInterface
     */
    public function capture($tnRef, $comment);

    /**
     * Authorize and capture request.
     *
     * @api
     * @param string $ccNum
     * @param string $ccHolder
     * @param string $ccExpMonth
     * @param string $ccExpYear
     * @param string $ccCvv
     * @param float $amount
     * @param string $currency
     * @param string $merchantTnRef
     * @param string $comment
     * @return \Mdg\PaymentGateway\Api\ResponseInterface
     */
    public function authcapture(
        $ccNum,
        $ccHolder,
        $ccExpMonth,
        $ccExpYear,
        $ccCvv,
        $amount,
        $currency,
        $merchantTnRef,
        $comment
    );

    /**
     * Void authorized transaction
     *
     * @api
     * @param string $tnRef
     * @return \Mdg\PaymentGateway\Api\ResponseInterface
     */
    public function void($tnRef);

    /**
     * Refund captured transaction
     *
     * @api
     * @param string $tnRefwebapi.xml
     * @param float $amount
     * @param string $currency
     * @return \Mdg\PaymentGateway\Api\ResponseInterface
     */
    public function refund($tnRef, $amount, $currency);
}
