<?php
/**
 * @author Misha Medzhytov
 * @copyright Copyright (c) 2011-2018 Mdg, Inc. (http://www.medzhytov.com)
 */
namespace Mdg\PaymentGateway\Api;

/**
 * Interface TransactionInterface
 *
 * @package Mdg\PaymentGateway\Api
 */
interface TransactionInterface
{
    const TYPE_AUTHORIZE = 'authorize';
    const TYPE_CAPTURE = 'capture';
    const TYPE_AUTHCAPTURE = 'authcapture';
    const TYPE_VOID = 'void';
    const TYPE_REFUND = 'refund';

    /**
     * @return string
     */
    public function getTnRef();

    /**
     * @param string $tnRef
     * @return void
     */
    public function setTnRef($tnRef);

    /**
     * @return string
     */
    public function getParentTnRef();

    /**
     * @param string $parentTnRef
     * @return void
     */
    public function setParentTnRef($parentTnRef);

    /**
     * @return string
     */
    public function getMerchantTnRef();

    /**
     * @param string $merchantTnRef
     * @return void
     */
    public function setMerchantTnRef($merchantTnRef);

    /**
     * @return float
     */
    public function getAmount();

    /**
     * @param float $amount
     * @return void
     */
    public function setAmount($amount);

    /**
     * @return string
     */
    public function getCurrency();

    /**
     * @param string $currency
     * @return void
     */
    public function setCurrency($currency);

    /**
     * @return string
     */
    public function getComment();

    /**
     * @param string $comment
     * @return void
     */
    public function setComment($comment);

    /**
     * @return string
     */
    public function getType();

    /**
     * @param string $type
     * @return void
     */
    public function setType($type);
}
