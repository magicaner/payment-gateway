<?php
/**
 * @author Misha Medzhytov
 * @copyright Copyright (c) 2011-2018 Mdg, Inc. (http://www.medzhytov.com)
 */
namespace Mdg\PaymentGateway\Api;

use Mdg\PaymentGateway\Model\Transaction;

/**
 * Interface GatewayClientInterface
 *
 * @package Mdg\PaymentGateway\Api
 */
interface GatewayClientInterface
{
    /**
     * @param string $ccNum
     * @param string $ccHolder
     * @param string $ccExpMonth
     * @param string $ccExpYear
     * @param string $ccCvv
     * @param float $amount
     * @param string $currency
     * @param string $merchantTnRef
     * @param string $comment
     * @return array
     */
    public function authorize(
        $ccNum,
        $ccHolder,
        $ccExpMonth,
        $ccExpYear,
        $ccCvv,
        $amount,
        $currency,
        $merchantTnRef,
        $comment
    );

    /**
     * @param string $tnRef
     * @param string $comment
     * @return array
     */
    public function capture($tnRef, $comment);

    /**
     * @param string $tnRef
     * @param float $amount
     * @param string $currency
     * @return array
     */
    public function refund($tnRef, $amount, $currency);

    /**
     * @param string $tnRef
     * @return array
     */
    public function void($tnRef);

    /**
     * @param string $ccNum
     * @param string $ccHolder
     * @param string $ccExpMonth
     * @param string $ccExpYear
     * @param string $ccCvv
     * @param float $amount
     * @param string $currency
     * @param string $merchantTnRef
     * @param string $comment
     * @return array
     */
    public function authcapture(
        $ccNum,
        $ccHolder,
        $ccExpMonth,
        $ccExpYear,
        $ccCvv,
        $amount,
        $currency,
        $merchantTnRef,
        $comment
    );
}
