<?php
/**
 * @author Misha Medzhytov
 * @copyright Copyright (c) 2011-2018 Mdg, Inc. (http://www.medzhytov.com)
 */
namespace Mdg\PaymentGateway\Api;

/**
 * Interface ResponseInterface
 *
 * @package Mdg\PaymentGateway\Api
 */
interface ResponseInterface
{
    /**
     * @return \Mdg\PaymentGateway\Api\TransactionInterface
     */
    public function getTransaction();

    /**
     * @param \Mdg\PaymentGateway\Api\TransactionInterface $transaction
     * @return void
     */
    public function setTransaction($transaction);

    /**
     * @param string $message
     * @return void
     */
    public function setMessage($message);

    /**
     * @return string
     */
    public function getMessage();

    /**
     * @param boolean $success
     * @return $this
     */
    public function setSuccess($success = true);

    /**
     * @return boolean
     */
    public function getSuccess();

    /**
     * @param string $code
     * @return $this
     */
    public function setCode($code);

    /**
     * @return string
     */
    public function getCode();
}
